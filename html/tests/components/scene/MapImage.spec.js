import 'jest-canvas-mock';

import { createLocalVue, shallowMount } from '@vue/test-utils';
import VueKonva from 'vue-konva';
import Vuex from 'vuex';

import MapImage from '@/table/components/scene/MapImage.vue';

const localVue = createLocalVue();
localVue.use(VueKonva);
localVue.use(Vuex);

const store = new Vuex.Store({
    modules: {
        editingModes: {
            namespaced: true,
            state: {
                editMap: false,
            },
        },
    },
});

describe('MapImage', () => {
    describe('imageConfig', () => {
        it('should not throw an error', () => {
            shallowMount(MapImage, {
                localVue,
                store,
            });
        });
    });
});
